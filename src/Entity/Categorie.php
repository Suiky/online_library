<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie", indexes={@ORM\Index(name="idcatégorie_1", columns={"idcatégorie_1"})})
 * @ORM\Entity
 */
class Categorie
{
    /**
     * @var string
     *
     * @ORM\Column(name="idcatégorie", type="string", length=42, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcatégorie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom_de_catégorie", type="string", length=42, nullable=true)
     */
    private $nomDeCatégorie;

    /**
     * @var \Categorie
     *
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcatégorie_1", referencedColumnName="idcatégorie")
     * })
     */
    private $idcatégorie1;

    public function getIdcatégorie(): ?string
    {
        return $this->idcatégorie;
    }

    public function getNomDeCatégorie(): ?string
    {
        return $this->nomDeCatégorie;
    }

    public function setNomDeCatégorie(?string $nomDeCatégorie): self
    {
        $this->nomDeCatégorie = $nomDeCatégorie;

        return $this;
    }

    public function getIdcatégorie1(): ?self
    {
        return $this->idcatégorie1;
    }

    public function setIdcatégorie1(?self $idcatégorie1): self
    {
        $this->idcatégorie1 = $idcatégorie1;

        return $this;
    }


}

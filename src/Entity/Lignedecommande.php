<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignedecommande
 *
 * @ORM\Table(name="lignedecommande", indexes={@ORM\Index(name="idcommande", columns={"idcommande"}), @ORM\Index(name="idlivre", columns={"idlivre"})})
 * @ORM\Entity
 */
class Lignedecommande
{
    /**
     * @var string
     *
     * @ORM\Column(name="idlignedecommande", type="string", length=42, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlignedecommande;

    /**
     * @var string|null
     *
     * @ORM\Column(name="quantité", type="string", length=42, nullable=true)
     */
    private $quantité;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prix_unitaire", type="string", length=42, nullable=true)
     */
    private $prixUnitaire;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prix_ht", type="string", length=42, nullable=true)
     */
    private $prixHt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prix_ttc", type="string", length=42, nullable=true)
     */
    private $prixTtc;

    /**
     * @var \Commande
     *
     * @ORM\ManyToOne(targetEntity="Commande")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcommande", referencedColumnName="idcommande")
     * })
     */
    private $idcommande;

    /**
     * @var \Livres
     *
     * @ORM\ManyToOne(targetEntity="Livres")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idlivre", referencedColumnName="idlivre")
     * })
     */
    private $idlivre;

    public function getIdlignedecommande(): ?string
    {
        return $this->idlignedecommande;
    }

    public function getQuantité(): ?string
    {
        return $this->quantité;
    }

    public function setQuantité(?string $quantité): self
    {
        $this->quantité = $quantité;

        return $this;
    }

    public function getPrixUnitaire(): ?string
    {
        return $this->prixUnitaire;
    }

    public function setPrixUnitaire(?string $prixUnitaire): self
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    public function getPrixHt(): ?string
    {
        return $this->prixHt;
    }

    public function setPrixHt(?string $prixHt): self
    {
        $this->prixHt = $prixHt;

        return $this;
    }

    public function getPrixTtc(): ?string
    {
        return $this->prixTtc;
    }

    public function setPrixTtc(?string $prixTtc): self
    {
        $this->prixTtc = $prixTtc;

        return $this;
    }

    public function getIdcommande(): ?Commande
    {
        return $this->idcommande;
    }

    public function setIdcommande(?Commande $idcommande): self
    {
        $this->idcommande = $idcommande;

        return $this;
    }

    public function getIdlivre(): ?Livres
    {
        return $this->idlivre;
    }

    public function setIdlivre(?Livres $idlivre): self
    {
        $this->idlivre = $idlivre;

        return $this;
    }


}

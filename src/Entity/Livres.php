<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Livres
 *
 * @ORM\Table(name="livres", indexes={@ORM\Index(name="idcatégorie", columns={"idcatégorie"})})
 * @ORM\Entity
 */
class Livres
{
    /**
     * @var string
     *
     * @ORM\Column(name="idlivre", type="string", length=42, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlivre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="auteur", type="string", length=42, nullable=true)
     */
    private $auteur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titre", type="string", length=42, nullable=true)
     */
    private $titre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sous-titre", type="string", length=42, nullable=true)
     */
    private $sousTitre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="éditeur", type="string", length=42, nullable=true)
     */
    private $éditeur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="isbn", type="string", length=42, nullable=true)
     */
    private $isbn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prix", type="string", length=42, nullable=true)
     */
    private $prix;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_de_la_couverture", type="string", length=42, nullable=true)
     */
    private $imageDeLaCouverture;

    /**
     * @var string|null
     *
     * @ORM\Column(name="résumé", type="string", length=42, nullable=true)
     */
    private $résumé;

    /**
     * @var \Categorie
     *
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcatégorie", referencedColumnName="idcatégorie")
     * })
     */
    private $idcatégorie;

    public function getIdlivre(): ?string
    {
        return $this->idlivre;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(?string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getSousTitre(): ?string
    {
        return $this->sousTitre;
    }

    public function setSousTitre(?string $sousTitre): self
    {
        $this->sousTitre = $sousTitre;

        return $this;
    }

    public function getéditeur(): ?string
    {
        return $this->éditeur;
    }

    public function setéditeur(?string $éditeur): self
    {
        $this->éditeur = $éditeur;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getImageDeLaCouverture(): ?string
    {
        return $this->imageDeLaCouverture;
    }

    public function setImageDeLaCouverture(?string $imageDeLaCouverture): self
    {
        $this->imageDeLaCouverture = $imageDeLaCouverture;

        return $this;
    }

    public function getRésumé(): ?string
    {
        return $this->résumé;
    }

    public function setRésumé(?string $résumé): self
    {
        $this->résumé = $résumé;

        return $this;
    }

    public function getIdcatégorie(): ?Categorie
    {
        return $this->idcatégorie;
    }

    public function setIdcatégorie(?Categorie $idcatégorie): self
    {
        $this->idcatégorie = $idcatégorie;

        return $this;
    }


}

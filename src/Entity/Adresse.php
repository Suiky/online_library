<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Adresse
 *
 * @ORM\Table(name="adresse")
 * @ORM\Entity
 */
class Adresse
{
    /**
     * @var string
     *
     * @ORM\Column(name="idadresse", type="string", length=42, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idadresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_de_voie", type="string", length=42, nullable=true)
     */
    private $nDeVoie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type_de_voie", type="string", length=42, nullable=true)
     */
    private $typeDeVoie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom_de_voie", type="string", length=42, nullable=true)
     */
    private $nomDeVoie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_postale", type="string", length=42, nullable=true)
     */
    private $codePostale;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville", type="string", length=42, nullable=true)
     */
    private $ville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_de_bâtiment", type="string", length=42, nullable=true)
     */
    private $nDeBâtiment;

    public function getIdadresse(): ?string
    {
        return $this->idadresse;
    }

    public function getNDeVoie(): ?string
    {
        return $this->nDeVoie;
    }

    public function setNDeVoie(?string $nDeVoie): self
    {
        $this->nDeVoie = $nDeVoie;

        return $this;
    }

    public function getTypeDeVoie(): ?string
    {
        return $this->typeDeVoie;
    }

    public function setTypeDeVoie(?string $typeDeVoie): self
    {
        $this->typeDeVoie = $typeDeVoie;

        return $this;
    }

    public function getNomDeVoie(): ?string
    {
        return $this->nomDeVoie;
    }

    public function setNomDeVoie(?string $nomDeVoie): self
    {
        $this->nomDeVoie = $nomDeVoie;

        return $this;
    }

    public function getCodePostale(): ?string
    {
        return $this->codePostale;
    }

    public function setCodePostale(?string $codePostale): self
    {
        $this->codePostale = $codePostale;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getNDeBâtiment(): ?string
    {
        return $this->nDeBâtiment;
    }

    public function setNDeBâtiment(?string $nDeBâtiment): self
    {
        $this->nDeBâtiment = $nDeBâtiment;

        return $this;
    }


}

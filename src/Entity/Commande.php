<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="commande", indexes={@ORM\Index(name="idadresse", columns={"idadresse"}), @ORM\Index(name="idutilisateur", columns={"idutilisateur"})})
 * @ORM\Entity
 */
class Commande
{
    /**
     * @var string
     *
     * @ORM\Column(name="idcommande", type="string", length=42, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcommande;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dateheure", type="string", length=42, nullable=true)
     */
    private $dateheure;

    /**
     * @var string|null
     *
     * @ORM\Column(name="idutilisateur_1", type="string", length=42, nullable=true)
     */
    private $idutilisateur1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="idcommande_1", type="string", length=42, nullable=true)
     */
    private $idcommande1;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idutilisateur", referencedColumnName="idutilisateur")
     * })
     */
    private $idutilisateur;

    /**
     * @var \Adresse
     *
     * @ORM\ManyToOne(targetEntity="Adresse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idadresse", referencedColumnName="idadresse")
     * })
     */
    private $idadresse;

    public function getIdcommande(): ?string
    {
        return $this->idcommande;
    }

    public function getDateheure(): ?string
    {
        return $this->dateheure;
    }

    public function setDateheure(?string $dateheure): self
    {
        $this->dateheure = $dateheure;

        return $this;
    }

    public function getIdutilisateur1(): ?string
    {
        return $this->idutilisateur1;
    }

    public function setIdutilisateur1(?string $idutilisateur1): self
    {
        $this->idutilisateur1 = $idutilisateur1;

        return $this;
    }

    public function getIdcommande1(): ?string
    {
        return $this->idcommande1;
    }

    public function setIdcommande1(?string $idcommande1): self
    {
        $this->idcommande1 = $idcommande1;

        return $this;
    }

    public function getIdutilisateur(): ?Utilisateur
    {
        return $this->idutilisateur;
    }

    public function setIdutilisateur(?Utilisateur $idutilisateur): self
    {
        $this->idutilisateur = $idutilisateur;

        return $this;
    }

    public function getIdadresse(): ?Adresse
    {
        return $this->idadresse;
    }

    public function setIdadresse(?Adresse $idadresse): self
    {
        $this->idadresse = $idadresse;

        return $this;
    }


}

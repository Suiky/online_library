<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur", indexes={@ORM\Index(name="idlivre", columns={"idlivre"})})
 * @ORM\Entity
 */
class Utilisateur
{
    /**
     * @var string
     *
     * @ORM\Column(name="idutilisateur", type="string", length=42, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idutilisateur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=42, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prénom", type="string", length=42, nullable=true)
     */
    private $prénom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_de_téléphone", type="string", length=42, nullable=true)
     */
    private $nDeTéléphone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=42, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="avis", type="string", length=42, nullable=true)
     */
    private $avis;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="string", length=42, nullable=true)
     */
    private $note;

    /**
     * @var \Livres
     *
     * @ORM\ManyToOne(targetEntity="Livres")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idlivre", referencedColumnName="idlivre")
     * })
     */
    private $idlivre;

    public function getIdutilisateur(): ?string
    {
        return $this->idutilisateur;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrénom(): ?string
    {
        return $this->prénom;
    }

    public function setPrénom(?string $prénom): self
    {
        $this->prénom = $prénom;

        return $this;
    }

    public function getNDeTéléphone(): ?string
    {
        return $this->nDeTéléphone;
    }

    public function setNDeTéléphone(?string $nDeTéléphone): self
    {
        $this->nDeTéléphone = $nDeTéléphone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAvis(): ?string
    {
        return $this->avis;
    }

    public function setAvis(?string $avis): self
    {
        $this->avis = $avis;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIdlivre(): ?Livres
    {
        return $this->idlivre;
    }

    public function setIdlivre(?Livres $idlivre): self
    {
        $this->idlivre = $idlivre;

        return $this;
    }


}

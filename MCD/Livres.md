**COMMANDE** (<ins>IDcommande</ins>, dateHeure, _IDadresse_, _IDutilisateur_, IDutilisateur.1, IDcommande.1)  
**UTILISATEUR** (<ins>IDutilisateur</ins>, Nom, prénom, N de téléphone, email, _IDLIVRE_, avis, note)  
**ADRESSE** (<ins>IDadresse</ins>, N de voie, TYPE DE voie, NOM de voie, code postale, ville, N de bâtiment)  
**CATEGORIE** (<ins>IDCatégorie</ins>, Nom de Catégorie, _IDCatégorie.1_)  
**LIGNEDECOMMANDE** (<ins>IDlignedecommande</ins>, quantité, prix unitaire, prix HT, prix TTC, _IDLIVRE_, _IDcommande_)  
**LIVRES** (<ins>IDLIVRE</ins>, Auteur, titre, sous-titre, éditeur, ISBN, prix, image de la couverture, résumé, _IDCatégorie_)
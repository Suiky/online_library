CREATE DATABASE IF NOT EXISTS `LIVRES` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `LIVRES`;

CREATE TABLE `COMMANDE` (
  `idcommande` VARCHAR(42),
  `dateheure` VARCHAR(42),
  `idadresse` VARCHAR(42),
  `idutilisateur` VARCHAR(42),
  `idutilisateur_1` VARCHAR(42),
  `idcommande_1` VARCHAR(42),
  PRIMARY KEY (`idcommande`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `UTILISATEUR` (
  `idutilisateur` VARCHAR(42),
  `nom` VARCHAR(42),
  `prénom` VARCHAR(42),
  `n_de_téléphone` VARCHAR(42),
  `email` VARCHAR(42),
  `idlivre` VARCHAR(42),
  `avis` VARCHAR(42),
  `note` VARCHAR(42),
  PRIMARY KEY (`idutilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ADRESSE` (
  `idadresse` VARCHAR(42),
  `n_de_voie` VARCHAR(42),
  `type_de_voie` VARCHAR(42),
  `nom_de_voie` VARCHAR(42),
  `code_postale` VARCHAR(42),
  `ville` VARCHAR(42),
  `n_de_bâtiment` VARCHAR(42),
  PRIMARY KEY (`idadresse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CATEGORIE` (
  `idcatégorie` VARCHAR(42),
  `nom_de_catégorie` VARCHAR(42),
  `idcatégorie_1` VARCHAR(42),
  PRIMARY KEY (`idcatégorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `LIGNEDECOMMANDE` (
  `idlignedecommande` VARCHAR(42),
  `quantité` VARCHAR(42),
  `prix_unitaire` VARCHAR(42),
  `prix_ht` VARCHAR(42),
  `prix_ttc` VARCHAR(42),
  `idlivre` VARCHAR(42),
  `idcommande` VARCHAR(42),
  PRIMARY KEY (`idlignedecommande`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `LIVRES` (
  `idlivre` VARCHAR(42),
  `auteur` VARCHAR(42),
  `titre` VARCHAR(42),
  `sous-titre` VARCHAR(42),
  `éditeur` VARCHAR(42),
  `isbn` VARCHAR(42),
  `prix` VARCHAR(42),
  `image_de_la_couverture` VARCHAR(42),
  `résumé` VARCHAR(42),
  `idcatégorie` VARCHAR(42),
  PRIMARY KEY (`idlivre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `COMMANDE` ADD FOREIGN KEY (`idutilisateur`) REFERENCES `UTILISATEUR` (`idutilisateur`);
ALTER TABLE `COMMANDE` ADD FOREIGN KEY (`idadresse`) REFERENCES `ADRESSE` (`idadresse`);
ALTER TABLE `UTILISATEUR` ADD FOREIGN KEY (`idlivre`) REFERENCES `LIVRES` (`idlivre`);
ALTER TABLE `CATEGORIE` ADD FOREIGN KEY (`idcatégorie_1`) REFERENCES `CATEGORIE` (`idcatégorie`);
ALTER TABLE `LIGNEDECOMMANDE` ADD FOREIGN KEY (`idcommande`) REFERENCES `COMMANDE` (`idcommande`);
ALTER TABLE `LIGNEDECOMMANDE` ADD FOREIGN KEY (`idlivre`) REFERENCES `LIVRES` (`idlivre`);
ALTER TABLE `LIVRES` ADD FOREIGN KEY (`idcatégorie`) REFERENCES `CATEGORIE` (`idcatégorie`);